<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Setting;
use Validator;
use App\Promocode;
use App\PromocodeUsage;
use Auth;

use App\ProviderService;
use App\Provider;
use App\Settings;
use App\ServiceType;
use Carbon\Carbon;
use App\UserComplaint;
use App\FareSetting;
use App\PeakAndNight;
use App\Reward;
use App\Redeem;
use App\Referral;

class AjaxHandlerController extends Controller
{
	
	protected $UserAPI;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserApiController $UserAPI)
    {
        $this->UserAPI = $UserAPI;
    }


  /**
     *  Apply Promo codes promo code.
     *
     * @return \Illuminate\Http\Response
     */

    public function applyPromoCodeOnEstimatedFare(Request $request) {
		
		$validator = Validator::make($request->all(), [
             'promo_code' => 'required|exists:promocodes,promo_code',
           
        ]);
		
		if ($validator->fails()) {
			
				return response()->json([
                        'errors' => $validator->getMessageBag()->toArray(), 
                        'code' => 'request_error'
                    ]);
					
			}
		
		
        try{

            $find_promo = Promocode::where('promo_code',$request->promo_code)->first();

            if($find_promo->status == 'EXPIRED' || (date("Y-m-d") > $find_promo->expiration)){

                if($request->ajax()){

                    return response()->json([
                        'message' => trans('api.promocode_expired'), 
                        'code' => 'promocode_expired'
                    ]);

                }else{
                    return back()->with('flash_error', trans('api.promocode_expired'));
                }

            }elseif(PromocodeUsage::where('promocode_id',$find_promo->id)->where('user_id', Auth::user()->id)->where('status','ADDED')->count() > 0){

                if($request->ajax()) {

                    return response()->json([
                        'message' => trans('api.promocode_already_in_use'), 
                        'code' => 'promocode_already_in_use'
                        ]);

                } else {
                    return back()->with('flash_error', 'Promocode Already in use');
                }

            } else {
				
                // apply promo_code on estimated fare
                $fare =   $this->getEstimatedFare( $request );
                
                if($request->ajax()){

                    return response()->json([
                            'message' 	=> trans('api.promocode_applied') ,
							'fare'		=> $fare,
                            'code' 		=> 'promocode_applied'
                         ]); 

                }else{
                    return back()->with('flash_success', trans('api.promocode_applied'));
                }
            }

        }

        catch (Exception $e) {
            if($request->ajax()){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something Went Wrong');
            }
        }
    }


    public function getEstimatedFare(Request $request ) {
        
        $fare = $this->UserAPI->estimated_fare($request)->getData();	
        $fare->estimated_fare = currency( $fare->estimated_fare );
        
        return $fare;
    
    }



    public function estimated_fare(Request $request) {
        
        //Log::info('Estimate', $request->all());
        $this->validate($request,[
            's_latitude' => 'required|numeric',
            's_longitude' => 'required|numeric',
            'd_latitude' => 'required|numeric',
            'd_longitude' => 'required|numeric',
            'service_type' => 'required|numeric|exists:service_types,id',
        ]);

        try{

            $details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$request->s_latitude.",".$request->s_longitude."&destinations=".$request->d_latitude.",".$request->d_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');

            $json = curl($details);

            $details = json_decode($json, TRUE);
            
            @$meter = $details['rows'][0]['elements'][0]['distance']['value'];
            @$time = $details['rows'][0]['elements'][0]['duration']['text'];
            @$seconds = $details['rows'][0]['elements'][0]['duration']['value'];

            @$kilometer = round($meter/1000);
            @$minutes = round($seconds/60);

            $tax_percentage = Setting::get('tax_percentage');
            $currency = Setting::get('currency');
            $commission_percentage = Setting::get('commission_percentage');
            $service_type = ServiceType::findOrFail($request->service_type);
			$total_discount = 0;
            $price = $service_type->fixed;
            $currentDay = date('l');
            $Time  = Carbon::now(env('APP_TIMEZONE'));
            $booking_time = $Time->toTimeString();
            //return $service_type;exit;

            
            // condition
      
		    $fareSetting = FareSetting::where('from_km','<=',round($kilometer,0))->where('upto_km','>=',round($kilometer,0))
		    	//->where('peak_hour','YES')
		   		->where('status',1)
		     	->orderBy('upto_km','DESC')
		     	->first();
		    if(!empty($fareSetting)){
		    	$peakAndNight  = new PeakAndNight;
		    	$peakAndNight = $peakAndNight->where('start_time','<=',$booking_time)
		    	->where('end_time','>=',$booking_time)
		    	->where('status',1);
		    	if($fareSetting->peak_hour=='YES' && $fareSetting->late_night=='YES'){
		    		$peakAndNight = $peakAndNight->where(function($q) use($currentDay){
		    			$q->where('day',$currentDay)
		    			  ->orWhere('day',null);
		    		});

		    	}else{
		    		$peakAndNight = $peakAndNight->where(['day'=>$currentDay]);	
		    	}
		    	$peakAndNight = $peakAndNight->where('fare_setting_id',$fareSetting->id);
		    	$peakAndNight = $peakAndNight->orderBy('id','DESC')
		    	->first();
		   
		    	if(!empty($peakAndNight)){
	
		    		$amount = (($service_type->fixed+($kilometer*$fareSetting->price_per_km))*1); //double price on two way ride
		    		$Commision     = $amount * ( $commission_percentage/100 );
		    		$extra_tax_price = ( $peakAndNight->fare_in_percentage/100 ) * $amount;
		    		$amount = $amount + $extra_tax_price;
		    		$tax_price = ( $tax_percentage/100 ) * $amount;
		    		$total = $amount + $Commision +$tax_price;	
		    	}else{
		    	    
		    			// fare setting applied without peak day and time
		    			 //dd($fareSetting->price_per_km);
		    			$amount = (($service_type->fixed+($kilometer*$fareSetting->price_per_km))*1); //double price on two way ride
		    			
		    			$Commision     = $amount * ( $commission_percentage/100 );
		    			$tax_price = ( $tax_percentage/100 ) * $amount;
		    			$total = $amount + $Commision + $tax_price;
		    	         
		    	}
		    }   else{
		        
				// else condition
	            if($service_type->calculator == 'MIN') {
	                $price += $service_type->minute * $minutes;
	            } else if($service_type->calculator == 'HOUR') {
	                $price += $service_type->minute * 60;
	            } else if($service_type->calculator == 'DISTANCE') {
	                $price += ($kilometer * $service_type->price);
	            } else if($service_type->calculator == 'DISTANCEMIN') {
	                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes);
	            } else if($service_type->calculator == 'DISTANCEHOUR') {
	                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes * 60);
	            } else {
	                $price += ($kilometer * $service_type->price);
	            }
	            $Commision     = $price * ( $commission_percentage/100 );
	            $tax_price = ( $tax_percentage/100 ) * $price;
	            $total = $price + $Commision + $tax_price;
				//
			
		    }		
      /////////////////////////////////////////////////////////////////////////////////////////////
			//sid
			if ( $request->has('promo_code') ) {
				// Apply  promo code
				if($promo_code =  Promocode::where('promo_code', $request->promo_code)->first() ) {
					$total_discount =  ($total * $promo_code->discount)/100;
					$total = $total - $total_discount; 
				}
			}
		    

            // shazz:
            // check if user have any active referral discount
           /* $referral_used = Referral::where(['user_id'=>Auth::user()->id,'referral_status'=>1])->first();
            if($referral_used!=null){
               $total_discount =  ($total * $referral_used->referral_discount)/100;
               $total = $total - $total_discount;  
            }*/
            /// end check

            $ActiveProviders = ProviderService::AvailableServiceProvider($request->service_type)->get()->pluck('provider_id');

            $distance = Setting::get('provider_search_radius', '10');
            $latitude = $request->s_latitude;
            $longitude = $request->s_longitude;

            $Providers = Provider::whereIn('id', $ActiveProviders)
                ->where('status', 'approved')
                ->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                ->get();

            $surge = 0;
            
            // if($Providers->count() <= Setting::get('surge_trigger') && $Providers->count() > 0){
            //     $surge_price = (Setting::get('surge_percentage')/100) * $total;
            //     $total += $surge_price;
            //     $surge = 1;
            // }
            
            //$Total= $total+$commission_percentage;
            return response()->json([
                    'estimated_fare' => currency( round($total,2) ), 
                    //'estimated_fare' => $total, 
                    'distance' => $kilometer,
                    'time' => $time,
                    //'surge' => $surge,
                    //'surge_value' => '1.4X',
                    'tax_price' => bcdiv($tax_price,1,2),
                    'base_price' => $service_type->fixed,
					'currency'   => $currency
                ]);

        }   catch(Exception $e) {
        	//return $e;
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

}

?>