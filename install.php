<?php
/*
-----------------------------------------------------------------------------------------------------------------
START OF CODE TO INCLUDE REQUIRED FILES. YOU SHOULD ADD THIS CODE TO YOUR SCRIPT.
-----------------------------------------------------------------------------------------------------------------
*/

//Load file with Auto PHP Licenser settings
require_once("bootstrap/exception/apl_core_configuration.php");

//Load file with Auto PHP Licenser functions
require_once("bootstrap/exception/apl_core_functions.php");


/*
-----------------------------------------------------------------------------------------------------------------
END OF CODE TO INCLUDE REQUIRED FILES. YOU SHOULD ADD THIS CODE TO YOUR SCRIPT.
-----------------------------------------------------------------------------------------------------------------
*/

/*
-----------------------------------------------------------------------------------------------------------------
START OF DEMO SCRIPT CODE. THIS CODE IS ONLY USED BY DEMO SCRIPT. YOU DO NOT NEED TO ADD THIS CODE TO YOUR SCRIPT.
-----------------------------------------------------------------------------------------------------------------
*/

//Get current url, so user doesn't need to enter it manually
$demo_page_url=str_ireplace('/install.php', '', 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

//Get all variables submitted by user
if (!empty($_POST) && is_array($_POST))
    {
    extract(array_map("trim", $_POST), EXTR_SKIP); //automatically make a variable from each argument submitted by user (don't overwrite existing variables)
    }

/*
-----------------------------------------------------------------------------------------------------------------
END OF DEMO SCRIPT CODE. THIS CODE IS ONLY USED BY DEMO SCRIPT. YOU DO NOT NEED TO ADD THIS CODE TO YOUR SCRIPT.
-----------------------------------------------------------------------------------------------------------------
*/

/*
-----------------------------------------------------------------------------------------------------------------
START OF REQUIRED AUTO PHP LICENSER INSTALLATION FUNCTIONS. YOU SHOULD ADD THIS CODE TO YOUR SCRIPT.
-----------------------------------------------------------------------------------------------------------------
*/

if (isset($submit_ok))
    {

    //Function can be provided with root URL of this script, licensed email address/license code and MySQLi link (only when database is used).
    //Function will return array with 'notification_case' and 'notification_text' keys, where 'notification_case' contains action status and 'notification_text' contains action summary.
   
        $ROOT_URL = $_POST['ROOT_URL'];
        $CLIENT_EMAIL = $_POST['CLIENT_EMAIL'];
        $LICENSE_CODE = $_POST['LICENSE_CODE'];
        $db_port=3306;
        $host=$_POST['host'];
        $username=$_POST['usr'];
        $password=$_POST['pass'];
        $db=$_POST['db'];
        $GLOBALS["mysqli"]=mysqli_connect($host,$username,$password,$db,$db_port); //establish connection to local MySQL database


    $license_notifications_array=aplInstallLicense($ROOT_URL, $CLIENT_EMAIL, $LICENSE_CODE,$GLOBALS["mysqli"]);
  
    if ($license_notifications_array['notification_case']=="notification_license_ok") //'notification_license_ok' case returned - operation succeeded
        {
        $demo_page_message="Installed and ready to use!";

        }
    else //Other case returned - operation failed
        {
        $demo_page_message=" Installation failed because of this reason: ".$license_notifications_array['notification_text'];
        }
    }

/*
-----------------------------------------------------------------------------------------------------------------
END OF REQUIRED AUTO PHP LICENSER INSTALLATION FUNCTIONS. YOU SHOULD ADD THIS CODE TO YOUR SCRIPT.
-----------------------------------------------------------------------------------------------------------------
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Install License </title>
</head>
<body>
    <?php if (!empty($demo_page_message)) {
        echo "<center><b>$demo_page_message</b></center><br><br>";
    } ?>
    <center><form action="<?php echo basename(__FILE__); ?>" method="post">
        <!-- Licensed email address (for personal license)<br> -->
        <input type="hidden" name="CLIENT_EMAIL" size="50">
         Database Host<br>
        <input type="text" name="host" size="100" value="127.0.0.1" required="required"><br><br>
        Database Name<br>
        <input type="text" name="db" size="100" required="required"><br><br>
        Database User Name<br>
        <input type="text" name="usr" size="100" required="required"><br><br>
        <br>
        Database Password<br>
        <input type="text" name="pass" size="100" required="required"><br><br>
        License code (for anonymous license)<br>
        <input type="text" name="LICENSE_CODE" size="50"><br><br>
        Installation URL (without / at the end)<br>
        <input type="text" name="ROOT_URL" size="50" value="<?php echo $demo_page_url; ?>"><br><br>
        <button type="submit" name="submit_ok">Submit</button><br>
    </form></center>
</body>
</html>
