<?php
/*
<<<<<<< HEAD
 * This file is part of the PHPUnit_MockObject package.
=======
 * This file is part of the phpunit-mock-objects package.
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

<<<<<<< HEAD
class Framework_MockBuilderTest extends PHPUnit_Framework_TestCase
=======
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockBuilder;

class MockBuilderTest extends TestCase
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
{
    public function testMockBuilderRequiresClassName()
    {
        $mock = $this->getMockBuilder(Mockable::class)->getMock();

<<<<<<< HEAD
        $this->assertTrue($mock instanceof Mockable);
=======
        $this->assertInstanceOf(Mockable::class, $mock);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    public function testByDefaultMocksAllMethods()
    {
        $mock = $this->getMockBuilder(Mockable::class)->getMock();

        $this->assertNull($mock->mockableMethod());
        $this->assertNull($mock->anotherMockableMethod());
    }

    public function testMethodsToMockCanBeSpecified()
    {
        $mock = $this->getMockBuilder(Mockable::class)
                     ->setMethods(['mockableMethod'])
                     ->getMock();

        $this->assertNull($mock->mockableMethod());
        $this->assertTrue($mock->anotherMockableMethod());
    }

    public function testMethodExceptionsToMockCanBeSpecified()
    {
        $mock = $this->getMockBuilder(Mockable::class)
            ->setMethodsExcept(['mockableMethod'])
            ->getMock();

        $this->assertTrue($mock->mockableMethod());
        $this->assertNull($mock->anotherMockableMethod());
    }

    public function testEmptyMethodExceptionsToMockCanBeSpecified()
    {
        $mock = $this->getMockBuilder(Mockable::class)
            ->setMethodsExcept()
            ->getMock();

        $this->assertNull($mock->mockableMethod());
        $this->assertNull($mock->anotherMockableMethod());
    }

    public function testByDefaultDoesNotPassArgumentsToTheConstructor()
    {
        $mock = $this->getMockBuilder(Mockable::class)->getMock();

        $this->assertEquals([null, null], $mock->constructorArgs);
    }

    public function testMockClassNameCanBeSpecified()
    {
        $mock = $this->getMockBuilder(Mockable::class)
                     ->setMockClassName('ACustomClassName')
                     ->getMock();

<<<<<<< HEAD
        $this->assertTrue($mock instanceof ACustomClassName);
=======
        $this->assertInstanceOf(ACustomClassName::class, $mock);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    public function testConstructorArgumentsCanBeSpecified()
    {
        $mock = $this->getMockBuilder(Mockable::class)
                     ->setConstructorArgs([23, 42])
                     ->getMock();

        $this->assertEquals([23, 42], $mock->constructorArgs);
    }

    public function testOriginalConstructorCanBeDisabled()
    {
        $mock = $this->getMockBuilder(Mockable::class)
                     ->disableOriginalConstructor()
                     ->getMock();

        $this->assertNull($mock->constructorArgs);
    }

    public function testByDefaultOriginalCloneIsPreserved()
    {
        $mock = $this->getMockBuilder(Mockable::class)
                     ->getMock();

        $cloned = clone $mock;

        $this->assertTrue($cloned->cloned);
    }

    public function testOriginalCloneCanBeDisabled()
    {
        $mock = $this->getMockBuilder(Mockable::class)
                     ->disableOriginalClone()
                     ->getMock();

        $mock->cloned = false;
        $cloned       = clone $mock;

        $this->assertFalse($cloned->cloned);
    }

    public function testProvidesAFluentInterface()
    {
        $spec = $this->getMockBuilder(Mockable::class)
                     ->setMethods(['mockableMethod'])
                     ->setConstructorArgs([])
                     ->setMockClassName('DummyClassName')
                     ->disableOriginalConstructor()
                     ->disableOriginalClone()
                     ->disableAutoload();

<<<<<<< HEAD
        $this->assertTrue($spec instanceof PHPUnit_Framework_MockObject_MockBuilder);
=======
        $this->assertInstanceOf(MockBuilder::class, $spec);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }
}
