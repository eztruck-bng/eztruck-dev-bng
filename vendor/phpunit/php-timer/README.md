[![Build Status](https://travis-ci.org/sebastianbergmann/php-timer.svg?branch=master)](https://travis-ci.org/sebastianbergmann/php-timer)

<<<<<<< HEAD
# PHP_Timer
=======
# phpunit/php-timer
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f

Utility class for timing things, factored out of PHPUnit into a stand-alone component.

## Installation

You can add this library as a local, per-project dependency to your project using [Composer](https://getcomposer.org/):

    composer require phpunit/php-timer

If you only need this library during development, for instance to run your project's test suite, then you should add it as a development-time dependency:

    composer require --dev phpunit/php-timer

## Usage

### Basic Timing

```php
<<<<<<< HEAD
PHP_Timer::start();

// ...

$time = PHP_Timer::stop();
var_dump($time);

print PHP_Timer::secondsToTimeString($time);
=======
use SebastianBergmann\Timer\Timer;

Timer::start();

// ...

$time = Timer::stop();
var_dump($time);

print Timer::secondsToTimeString($time);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
```

The code above yields the output below:

    double(1.0967254638672E-5)
    0 ms

### Resource Consumption Since PHP Startup

```php
<<<<<<< HEAD
print PHP_Timer::resourceUsage();
=======
use SebastianBergmann\Timer\Timer;

print Timer::resourceUsage();
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
```

The code above yields the output below:

    Time: 0 ms, Memory: 0.50MB
