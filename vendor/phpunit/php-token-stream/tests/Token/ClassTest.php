<?php
/*
 * This file is part of php-token-stream.
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use PHPUnit\Framework\TestCase;

class PHP_Token_ClassTest extends TestCase
{
    /**
     * @var PHP_Token_CLASS
     */
    private $class;

    /**
     * @var PHP_Token_FUNCTION
     */
    private $function;

    protected function setUp()
    {
<<<<<<< HEAD
        $ts = new PHP_Token_Stream(TEST_FILES_PATH . 'source2.php');

        foreach ($ts as $token) {
=======
        foreach (new PHP_Token_Stream(TEST_FILES_PATH . 'source2.php') as $token) {
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
            if ($token instanceof PHP_Token_CLASS) {
                $this->class = $token;
            }

            if ($token instanceof PHP_Token_FUNCTION) {
                $this->function = $token;
                break;
            }
        }
    }

<<<<<<< HEAD
    /**
     * @covers PHP_Token_CLASS::getKeywords
     */
=======
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    public function testGetClassKeywords()
    {
        $this->assertEquals('abstract', $this->class->getKeywords());
    }

<<<<<<< HEAD
    /**
     * @covers PHP_Token_FUNCTION::getKeywords
     */
=======
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    public function testGetFunctionKeywords()
    {
        $this->assertEquals('abstract,static', $this->function->getKeywords());
    }

<<<<<<< HEAD
    /**
     * @covers PHP_Token_FUNCTION::getVisibility
     */
=======
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    public function testGetFunctionVisibility()
    {
        $this->assertEquals('public', $this->function->getVisibility());
    }

    public function testIssue19()
    {
<<<<<<< HEAD
        $ts = new PHP_Token_Stream(TEST_FILES_PATH . 'issue19.php');

        foreach ($ts as $token) {
=======
        foreach (new PHP_Token_Stream(TEST_FILES_PATH . 'issue19.php') as $token) {
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
            if ($token instanceof PHP_Token_CLASS) {
                $this->assertFalse($token->hasInterfaces());
            }
        }
    }

    public function testIssue30()
    {
        $ts = new PHP_Token_Stream(TEST_FILES_PATH . 'issue30.php');
        $this->assertCount(1, $ts->getClasses());
    }

    public function testAnonymousClassesAreHandledCorrectly()
    {
        $ts = new PHP_Token_Stream(TEST_FILES_PATH . 'class_with_method_that_declares_anonymous_class.php');

        $classes = $ts->getClasses();

        $this->assertEquals(
            [
                'class_with_method_that_declares_anonymous_class',
                'AnonymousClass:9#31',
                'AnonymousClass:10#55',
                'AnonymousClass:11#75',
                'AnonymousClass:12#91',
                'AnonymousClass:13#107'
            ],
            array_keys($classes)
        );
    }

    /**
     * @ticket https://github.com/sebastianbergmann/php-token-stream/issues/52
     */
    public function testAnonymousClassesAreHandledCorrectly2()
    {
        $ts = new PHP_Token_Stream(TEST_FILES_PATH . 'class_with_method_that_declares_anonymous_class2.php');

        $classes = $ts->getClasses();

        $this->assertEquals(['Test', 'AnonymousClass:4#23'], array_keys($classes));
        $this->assertEquals(['methodOne', 'methodTwo'], array_keys($classes['Test']['methods']));

        $this->assertEmpty($ts->getFunctions());
    }

    public function testImportedFunctionsAreHandledCorrectly()
    {
        $ts = new PHP_Token_Stream(TEST_FILES_PATH . 'classUsesNamespacedFunction.php');

        $this->assertEmpty($ts->getFunctions());
        $this->assertCount(1, $ts->getClasses());
    }

    /**
     * @ticket https://github.com/sebastianbergmann/php-code-coverage/issues/543
     */
    public function testClassWithMultipleAnonymousClassesAndFunctionsIsHandledCorrectly()
    {
        $ts = new PHP_Token_Stream(TEST_FILES_PATH . 'class_with_multiple_anonymous_classes_and_functions.php');

        $classes = $ts->getClasses();

        $this->assertArrayHasKey('class_with_multiple_anonymous_classes_and_functions', $classes);
        $this->assertArrayHasKey('AnonymousClass:6#23', $classes);
        $this->assertArrayHasKey('AnonymousClass:12#53', $classes);
        $this->assertArrayHasKey('m', $classes['class_with_multiple_anonymous_classes_and_functions']['methods']);
        $this->assertArrayHasKey('anonymousFunction:18#81', $classes['class_with_multiple_anonymous_classes_and_functions']['methods']);
        $this->assertArrayHasKey('anonymousFunction:22#108', $classes['class_with_multiple_anonymous_classes_and_functions']['methods']);
    }

    /**
     * @ticket https://github.com/sebastianbergmann/php-token-stream/issues/68
     */
    public function testClassWithMethodNamedEmptyIsHandledCorrectly()
    {
<<<<<<< HEAD
        $ts = new PHP_Token_Stream(TEST_FILES_PATH . 'class_with_method_named_empty.php');

        $classes = $ts->getClasses();
=======
        $classes = (new PHP_Token_Stream(TEST_FILES_PATH . 'class_with_method_named_empty.php'))->getClasses();
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f

        $this->assertArrayHasKey('class_with_method_named_empty', $classes);
        $this->assertArrayHasKey('empty', $classes['class_with_method_named_empty']['methods']);
    }

    /**
     * @ticket https://github.com/sebastianbergmann/php-code-coverage/issues/424
     */
<<<<<<< HEAD
    public function testSomething()
    {
        $ts = new PHP_Token_Stream(TEST_FILES_PATH . 'php-code-coverage-issue-424.php');

        $classes = $ts->getClasses();
=======
    public function testAnonymousFunctionDoesNotAffectStartAndEndLineOfMethod()
    {
        $classes = (new PHP_Token_Stream(TEST_FILES_PATH . 'php-code-coverage-issue-424.php'))->getClasses();
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f

        $this->assertSame(5, $classes['Example']['methods']['even']['startLine']);
        $this->assertSame(12, $classes['Example']['methods']['even']['endLine']);

        $this->assertSame(7, $classes['Example']['methods']['anonymousFunction:7#28']['startLine']);
        $this->assertSame(9, $classes['Example']['methods']['anonymousFunction:7#28']['endLine']);
    }
}
