# GlobalState

Snapshotting of global state, factored out of PHPUnit into a stand-alone component.

[![Build Status](https://travis-ci.org/sebastianbergmann/global-state.svg?branch=master)](https://travis-ci.org/sebastianbergmann/global-state)

## Installation

<<<<<<< HEAD
To add this package as a local, per-project dependency to your project, simply add a dependency on `sebastian/global-state` to your project's `composer.json` file. Here is a minimal example of a `composer.json` file that just defines a dependency on GlobalState:

    {
        "require": {
            "sebastian/global-state": "1.0.*"
        }
    }
=======
You can add this library as a local, per-project dependency to your project using [Composer](https://getcomposer.org/):

    composer require sebastian/global-state

If you only need this library during development, for instance to run your project's test suite, then you should add it as a development-time dependency:

    composer require --dev sebastian/global-state

>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
