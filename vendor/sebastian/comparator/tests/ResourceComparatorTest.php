<?php
/*
<<<<<<< HEAD
 * This file is part of the Comparator package.
=======
 * This file is part of sebastian/comparator.
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\Comparator;

<<<<<<< HEAD
/**
 * @coversDefaultClass SebastianBergmann\Comparator\ResourceComparator
 *
 */
class ResourceComparatorTest extends \PHPUnit_Framework_TestCase
=======
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass SebastianBergmann\Comparator\ResourceComparator
 *
 * @uses SebastianBergmann\Comparator\Comparator
 * @uses SebastianBergmann\Comparator\Factory
 * @uses SebastianBergmann\Comparator\ComparisonFailure
 */
class ResourceComparatorTest extends TestCase
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
{
    private $comparator;

    protected function setUp()
    {
        $this->comparator = new ResourceComparator;
    }

    public function acceptsSucceedsProvider()
    {
<<<<<<< HEAD
        $tmpfile1 = tmpfile();
        $tmpfile2 = tmpfile();

        return array(
          array($tmpfile1, $tmpfile1),
          array($tmpfile2, $tmpfile2),
          array($tmpfile1, $tmpfile2)
        );
=======
        $tmpfile1 = \tmpfile();
        $tmpfile2 = \tmpfile();

        return [
          [$tmpfile1, $tmpfile1],
          [$tmpfile2, $tmpfile2],
          [$tmpfile1, $tmpfile2]
        ];
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    public function acceptsFailsProvider()
    {
<<<<<<< HEAD
        $tmpfile1 = tmpfile();

        return array(
          array($tmpfile1, null),
          array(null, $tmpfile1),
          array(null, null)
        );
=======
        $tmpfile1 = \tmpfile();

        return [
          [$tmpfile1, null],
          [null, $tmpfile1],
          [null, null]
        ];
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    public function assertEqualsSucceedsProvider()
    {
<<<<<<< HEAD
        $tmpfile1 = tmpfile();
        $tmpfile2 = tmpfile();

        return array(
          array($tmpfile1, $tmpfile1),
          array($tmpfile2, $tmpfile2)
        );
=======
        $tmpfile1 = \tmpfile();
        $tmpfile2 = \tmpfile();

        return [
          [$tmpfile1, $tmpfile1],
          [$tmpfile2, $tmpfile2]
        ];
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    public function assertEqualsFailsProvider()
    {
<<<<<<< HEAD
        $tmpfile1 = tmpfile();
        $tmpfile2 = tmpfile();

        return array(
          array($tmpfile1, $tmpfile2),
          array($tmpfile2, $tmpfile1)
        );
=======
        $tmpfile1 = \tmpfile();
        $tmpfile2 = \tmpfile();

        return [
          [$tmpfile1, $tmpfile2],
          [$tmpfile2, $tmpfile1]
        ];
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    /**
     * @covers       ::accepts
     * @dataProvider acceptsSucceedsProvider
     */
    public function testAcceptsSucceeds($expected, $actual)
    {
        $this->assertTrue(
          $this->comparator->accepts($expected, $actual)
        );
    }

    /**
     * @covers       ::accepts
     * @dataProvider acceptsFailsProvider
     */
    public function testAcceptsFails($expected, $actual)
    {
        $this->assertFalse(
          $this->comparator->accepts($expected, $actual)
        );
    }

    /**
     * @covers       ::assertEquals
     * @dataProvider assertEqualsSucceedsProvider
     */
    public function testAssertEqualsSucceeds($expected, $actual)
    {
        $exception = null;

        try {
            $this->comparator->assertEquals($expected, $actual);
<<<<<<< HEAD
        }

        catch (ComparisonFailure $exception) {
=======
        } catch (ComparisonFailure $exception) {
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        }

        $this->assertNull($exception, 'Unexpected ComparisonFailure');
    }

    /**
     * @covers       ::assertEquals
     * @dataProvider assertEqualsFailsProvider
     */
    public function testAssertEqualsFails($expected, $actual)
    {
<<<<<<< HEAD
        $this->setExpectedException('SebastianBergmann\\Comparator\\ComparisonFailure');
=======
        $this->expectException(ComparisonFailure::class);

>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        $this->comparator->assertEquals($expected, $actual);
    }
}
