<?php
/*
<<<<<<< HEAD
 * This file is part of the Comparator package.
=======
 * This file is part of sebastian/comparator.
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\Comparator;

use DateTime;
use DateTimeImmutable;
use DateTimeZone;
<<<<<<< HEAD
=======
use PHPUnit\Framework\TestCase;
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f

/**
 * @coversDefaultClass SebastianBergmann\Comparator\DateTimeComparator
 *
<<<<<<< HEAD
 */
class DateTimeComparatorTest extends \PHPUnit_Framework_TestCase
{
=======
 * @uses SebastianBergmann\Comparator\Comparator
 * @uses SebastianBergmann\Comparator\Factory
 * @uses SebastianBergmann\Comparator\ComparisonFailure
 */
class DateTimeComparatorTest extends TestCase
{
    /**
     * @var DateTimeComparator
     */
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    private $comparator;

    protected function setUp()
    {
        $this->comparator = new DateTimeComparator;
    }

    public function acceptsFailsProvider()
    {
        $datetime = new DateTime;

<<<<<<< HEAD
        return array(
          array($datetime, null),
          array(null, $datetime),
          array(null, null)
        );
=======
        return [
          [$datetime, null],
          [null, $datetime],
          [null, null]
        ];
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    public function assertEqualsSucceedsProvider()
    {
<<<<<<< HEAD
        return array(
          array(
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York'))
          ),
          array(
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 04:13:25', new DateTimeZone('America/New_York')),
            10
          ),
          array(
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 04:14:40', new DateTimeZone('America/New_York')),
            65
          ),
          array(
            new DateTime('2013-03-29', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29', new DateTimeZone('America/New_York'))
          ),
          array(
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 03:13:35', new DateTimeZone('America/Chicago'))
          ),
          array(
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 03:13:49', new DateTimeZone('America/Chicago')),
            15
          ),
          array(
            new DateTime('2013-03-30', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 23:00:00', new DateTimeZone('America/Chicago'))
          ),
          array(
            new DateTime('2013-03-30', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 23:01:30', new DateTimeZone('America/Chicago')),
            100
          ),
          array(
            new DateTime('@1364616000'),
            new DateTime('2013-03-29 23:00:00', new DateTimeZone('America/Chicago'))
          ),
          array(
            new DateTime('2013-03-29T05:13:35-0500'),
            new DateTime('2013-03-29T04:13:35-0600')
          )
        );
=======
        return [
            [
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 04:13:25', new DateTimeZone('America/New_York')),
                10
            ],
            [
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 04:14:40', new DateTimeZone('America/New_York')),
                65
            ],
            [
                new DateTime('2013-03-29', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29', new DateTimeZone('America/New_York'))
            ],
            [
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 03:13:35', new DateTimeZone('America/Chicago'))
            ],
            [
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 03:13:49', new DateTimeZone('America/Chicago')),
                15
            ],
            [
                new DateTime('2013-03-30', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 23:00:00', new DateTimeZone('America/Chicago'))
            ],
            [
                new DateTime('2013-03-30', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 23:01:30', new DateTimeZone('America/Chicago')),
                100
            ],
            [
                new DateTime('@1364616000'),
                new DateTime('2013-03-29 23:00:00', new DateTimeZone('America/Chicago'))
            ],
            [
                new DateTime('2013-03-29T05:13:35-0500'),
                new DateTime('2013-03-29T04:13:35-0600')
            ],
            [
                new DateTimeImmutable('2013-03-30', new DateTimeZone('America/New_York')),
                new DateTimeImmutable('2013-03-29 23:01:30', new DateTimeZone('America/Chicago')),
                100
            ],
        ];
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    public function assertEqualsFailsProvider()
    {
<<<<<<< HEAD
        return array(
          array(
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 03:13:35', new DateTimeZone('America/New_York'))
          ),
          array(
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 03:13:35', new DateTimeZone('America/New_York')),
            3500
          ),
          array(
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 05:13:35', new DateTimeZone('America/New_York')),
            3500
          ),
          array(
            new DateTime('2013-03-29', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-30', new DateTimeZone('America/New_York'))
          ),
          array(
            new DateTime('2013-03-29', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-30', new DateTimeZone('America/New_York')),
            43200
          ),
          array(
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/Chicago')),
          ),
          array(
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/Chicago')),
            3500
          ),
          array(
            new DateTime('2013-03-30', new DateTimeZone('America/New_York')),
            new DateTime('2013-03-30', new DateTimeZone('America/Chicago'))
          ),
          array(
            new DateTime('2013-03-29T05:13:35-0600'),
            new DateTime('2013-03-29T04:13:35-0600')
          ),
          array(
            new DateTime('2013-03-29T05:13:35-0600'),
            new DateTime('2013-03-29T05:13:35-0500')
          ),
        );
=======
        return [
            [
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 03:13:35', new DateTimeZone('America/New_York'))
            ],
            [
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 03:13:35', new DateTimeZone('America/New_York')),
                3500
            ],
            [
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 05:13:35', new DateTimeZone('America/New_York')),
                3500
            ],
            [
                new DateTime('2013-03-29', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-30', new DateTimeZone('America/New_York'))
            ],
            [
                new DateTime('2013-03-29', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-30', new DateTimeZone('America/New_York')),
                43200
            ],
            [
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/Chicago')),
            ],
            [
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/Chicago')),
                3500
            ],
            [
                new DateTime('2013-03-30', new DateTimeZone('America/New_York')),
                new DateTime('2013-03-30', new DateTimeZone('America/Chicago'))
            ],
            [
                new DateTime('2013-03-29T05:13:35-0600'),
                new DateTime('2013-03-29T04:13:35-0600')
            ],
            [
                new DateTime('2013-03-29T05:13:35-0600'),
                new DateTime('2013-03-29T05:13:35-0500')
            ],
        ];
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    /**
     * @covers  ::accepts
     */
    public function testAcceptsSucceeds()
    {
        $this->assertTrue(
<<<<<<< HEAD
          $this->comparator->accepts(
            new DateTime,
            new DateTime
          )
=======
            $this->comparator->accepts(
                new DateTime,
                new DateTime
            )
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        );
    }

    /**
     * @covers       ::accepts
     * @dataProvider acceptsFailsProvider
     */
    public function testAcceptsFails($expected, $actual)
    {
        $this->assertFalse(
<<<<<<< HEAD
          $this->comparator->accepts($expected, $actual)
=======
            $this->comparator->accepts($expected, $actual)
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        );
    }

    /**
     * @covers       ::assertEquals
     * @dataProvider assertEqualsSucceedsProvider
     */
    public function testAssertEqualsSucceeds($expected, $actual, $delta = 0.0)
    {
        $exception = null;

        try {
            $this->comparator->assertEquals($expected, $actual, $delta);
<<<<<<< HEAD
        }

        catch (ComparisonFailure $exception) {
=======
        } catch (ComparisonFailure $exception) {
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        }

        $this->assertNull($exception, 'Unexpected ComparisonFailure');
    }

    /**
     * @covers       ::assertEquals
     * @dataProvider assertEqualsFailsProvider
     */
    public function testAssertEqualsFails($expected, $actual, $delta = 0.0)
    {
<<<<<<< HEAD
        $this->setExpectedException(
          'SebastianBergmann\\Comparator\\ComparisonFailure',
          'Failed asserting that two DateTime objects are equal.'
        );
=======
        $this->expectException(ComparisonFailure::class);
        $this->expectExceptionMessage('Failed asserting that two DateTime objects are equal.');

>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        $this->comparator->assertEquals($expected, $actual, $delta);
    }

    /**
     * @requires PHP 5.5
     * @covers   ::accepts
     */
    public function testAcceptsDateTimeInterface()
    {
        $this->assertTrue($this->comparator->accepts(new DateTime, new DateTimeImmutable));
    }

    /**
     * @requires PHP 5.5
     * @covers   ::assertEquals
     */
    public function testSupportsDateTimeInterface()
    {
<<<<<<< HEAD
        $this->comparator->assertEquals(
          new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
          new DateTimeImmutable('2013-03-29 04:13:35', new DateTimeZone('America/New_York'))
=======
        $this->assertNull(
            $this->comparator->assertEquals(
                new DateTime('2013-03-29 04:13:35', new DateTimeZone('America/New_York')),
                new DateTimeImmutable('2013-03-29 04:13:35', new DateTimeZone('America/New_York'))
            )
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        );
    }
}
