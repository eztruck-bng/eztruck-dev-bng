<?php
/*
<<<<<<< HEAD
 * This file is part of the Comparator package.
=======
 * This file is part of sebastian/comparator.
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\Comparator;

<<<<<<< HEAD
=======
use PHPUnit\Framework\TestCase;
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
use stdClass;

/**
 * @coversDefaultClass SebastianBergmann\Comparator\TypeComparator
 *
<<<<<<< HEAD
 */
class TypeComparatorTest extends \PHPUnit_Framework_TestCase
=======
 * @uses SebastianBergmann\Comparator\Comparator
 * @uses SebastianBergmann\Comparator\Factory
 * @uses SebastianBergmann\Comparator\ComparisonFailure
 */
class TypeComparatorTest extends TestCase
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
{
    private $comparator;

    protected function setUp()
    {
        $this->comparator = new TypeComparator;
    }

    public function acceptsSucceedsProvider()
    {
<<<<<<< HEAD
        return array(
          array(true, 1),
          array(false, array(1)),
          array(null, new stdClass),
          array(1.0, 5),
          array("", "")
        );
=======
        return [
          [true, 1],
          [false, [1]],
          [null, new stdClass],
          [1.0, 5],
          ['', '']
        ];
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    public function assertEqualsSucceedsProvider()
    {
<<<<<<< HEAD
        return array(
          array(true, true),
          array(true, false),
          array(false, false),
          array(null, null),
          array(new stdClass, new stdClass),
          array(0, 0),
          array(1.0, 2.0),
          array("hello", "world"),
          array("", ""),
          array(array(), array(1,2,3))
        );
=======
        return [
          [true, true],
          [true, false],
          [false, false],
          [null, null],
          [new stdClass, new stdClass],
          [0, 0],
          [1.0, 2.0],
          ['hello', 'world'],
          ['', ''],
          [[], [1,2,3]]
        ];
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    public function assertEqualsFailsProvider()
    {
<<<<<<< HEAD
        return array(
          array(true, null),
          array(null, false),
          array(1.0, 0),
          array(new stdClass, array()),
          array("1", 1)
        );
=======
        return [
          [true, null],
          [null, false],
          [1.0, 0],
          [new stdClass, []],
          ['1', 1]
        ];
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    /**
     * @covers       ::accepts
     * @dataProvider acceptsSucceedsProvider
     */
    public function testAcceptsSucceeds($expected, $actual)
    {
        $this->assertTrue(
          $this->comparator->accepts($expected, $actual)
        );
    }

    /**
     * @covers       ::assertEquals
     * @dataProvider assertEqualsSucceedsProvider
     */
    public function testAssertEqualsSucceeds($expected, $actual)
    {
        $exception = null;

        try {
            $this->comparator->assertEquals($expected, $actual);
<<<<<<< HEAD
        }

        catch (ComparisonFailure $exception) {
=======
        } catch (ComparisonFailure $exception) {
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        }

        $this->assertNull($exception, 'Unexpected ComparisonFailure');
    }

    /**
     * @covers       ::assertEquals
     * @dataProvider assertEqualsFailsProvider
     */
    public function testAssertEqualsFails($expected, $actual)
    {
<<<<<<< HEAD
        $this->setExpectedException('SebastianBergmann\\Comparator\\ComparisonFailure', 'does not match expected type');
=======
        $this->expectException(ComparisonFailure::class);
        $this->expectExceptionMessage('does not match expected type');

>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        $this->comparator->assertEquals($expected, $actual);
    }
}
