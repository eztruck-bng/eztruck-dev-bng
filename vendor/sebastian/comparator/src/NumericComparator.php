<?php
/*
<<<<<<< HEAD
 * This file is part of the Comparator package.
=======
 * This file is part of sebastian/comparator.
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\Comparator;

/**
 * Compares numerical values for equality.
 */
class NumericComparator extends ScalarComparator
{
    /**
     * Returns whether the comparator can compare two values.
     *
<<<<<<< HEAD
     * @param  mixed $expected The first value to compare
     * @param  mixed $actual   The second value to compare
=======
     * @param mixed $expected The first value to compare
     * @param mixed $actual   The second value to compare
     *
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
     * @return bool
     */
    public function accepts($expected, $actual)
    {
        // all numerical values, but not if one of them is a double
        // or both of them are strings
<<<<<<< HEAD
        return is_numeric($expected) && is_numeric($actual) &&
               !(is_double($expected) || is_double($actual)) &&
               !(is_string($expected) && is_string($actual));
=======
        return \is_numeric($expected) && \is_numeric($actual) &&
               !(\is_float($expected) || \is_float($actual)) &&
               !(\is_string($expected) && \is_string($actual));
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }

    /**
     * Asserts that two values are equal.
     *
     * @param mixed $expected     First value to compare
     * @param mixed $actual       Second value to compare
     * @param float $delta        Allowed numerical distance between two values to consider them equal
     * @param bool  $canonicalize Arrays are sorted before comparison when set to true
     * @param bool  $ignoreCase   Case is ignored when set to true
     *
     * @throws ComparisonFailure
     */
    public function assertEquals($expected, $actual, $delta = 0.0, $canonicalize = false, $ignoreCase = false)
    {
<<<<<<< HEAD
        if (is_infinite($actual) && is_infinite($expected)) {
            return;
        }

        if ((is_infinite($actual) xor is_infinite($expected)) ||
            (is_nan($actual) or is_nan($expected)) ||
            abs($actual - $expected) > $delta) {
=======
        if (\is_infinite($actual) && \is_infinite($expected)) {
            return;
        }

        if ((\is_infinite($actual) xor \is_infinite($expected)) ||
            (\is_nan($actual) or \is_nan($expected)) ||
            \abs($actual - $expected) > $delta) {
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
            throw new ComparisonFailure(
                $expected,
                $actual,
                '',
                '',
                false,
<<<<<<< HEAD
                sprintf(
=======
                \sprintf(
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
                    'Failed asserting that %s matches expected %s.',
                    $this->exporter->export($actual),
                    $this->exporter->export($expected)
                )
            );
        }
    }
}
