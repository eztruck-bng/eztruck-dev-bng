<<<<<<< HEAD
<?php
=======
<?php declare(strict_types=1);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
/*
 * This file is part of sebastian/diff.
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\Diff;

use PHPUnit\Framework\TestCase;

/**
 * @covers SebastianBergmann\Diff\Line
 */
<<<<<<< HEAD
class LineTest extends TestCase
=======
final class LineTest extends TestCase
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
{
    /**
     * @var Line
     */
    private $line;

<<<<<<< HEAD
    protected function setUp()
=======
    protected function setUp(): void
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        $this->line = new Line;
    }

<<<<<<< HEAD
    public function testCanBeCreatedWithoutArguments()
    {
        $this->assertInstanceOf('SebastianBergmann\Diff\Line', $this->line);
    }

    public function testTypeCanBeRetrieved()
    {
        $this->assertEquals(Line::UNCHANGED, $this->line->getType());
    }

    public function testContentCanBeRetrieved()
    {
        $this->assertEquals('', $this->line->getContent());
=======
    public function testCanBeCreatedWithoutArguments(): void
    {
        $this->assertInstanceOf(Line::class, $this->line);
    }

    public function testTypeCanBeRetrieved(): void
    {
        $this->assertSame(Line::UNCHANGED, $this->line->getType());
    }

    public function testContentCanBeRetrieved(): void
    {
        $this->assertSame('', $this->line->getContent());
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }
}
