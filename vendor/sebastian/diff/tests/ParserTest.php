<<<<<<< HEAD
<?php
=======
<?php declare(strict_types=1);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
/*
 * This file is part of sebastian/diff.
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\Diff;

use PHPUnit\Framework\TestCase;
<<<<<<< HEAD
=======
use SebastianBergmann\Diff\Utils\FileUtils;
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f

/**
 * @covers SebastianBergmann\Diff\Parser
 *
 * @uses SebastianBergmann\Diff\Chunk
 * @uses SebastianBergmann\Diff\Diff
 * @uses SebastianBergmann\Diff\Line
 */
<<<<<<< HEAD
class ParserTest extends TestCase
=======
final class ParserTest extends TestCase
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
{
    /**
     * @var Parser
     */
    private $parser;

<<<<<<< HEAD
    protected function setUp()
=======
    protected function setUp(): void
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        $this->parser = new Parser;
    }

<<<<<<< HEAD
    public function testParse()
    {
        $content = \file_get_contents(__DIR__ . '/fixtures/patch.txt');

        $diffs = $this->parser->parse($content);

        $this->assertInternalType('array', $diffs);
        $this->assertContainsOnlyInstancesOf('SebastianBergmann\Diff\Diff', $diffs);
        $this->assertCount(1, $diffs);

        $chunks = $diffs[0]->getChunks();
        $this->assertInternalType('array', $chunks);
        $this->assertContainsOnlyInstancesOf('SebastianBergmann\Diff\Chunk', $chunks);

        $this->assertCount(1, $chunks);

        $this->assertEquals(20, $chunks[0]->getStart());
=======
    public function testParse(): void
    {
        $content = FileUtils::getFileContent(__DIR__ . '/fixtures/patch.txt');

        $diffs = $this->parser->parse($content);

        $this->assertContainsOnlyInstancesOf(Diff::class, $diffs);
        $this->assertCount(1, $diffs);

        $chunks = $diffs[0]->getChunks();
        $this->assertContainsOnlyInstancesOf(Chunk::class, $chunks);

        $this->assertCount(1, $chunks);

        $this->assertSame(20, $chunks[0]->getStart());
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f

        $this->assertCount(4, $chunks[0]->getLines());
    }

<<<<<<< HEAD
    public function testParseWithMultipleChunks()
    {
        $content = \file_get_contents(__DIR__ . '/fixtures/patch2.txt');
=======
    public function testParseWithMultipleChunks(): void
    {
        $content = FileUtils::getFileContent(__DIR__ . '/fixtures/patch2.txt');
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f

        $diffs = $this->parser->parse($content);

        $this->assertCount(1, $diffs);

        $chunks = $diffs[0]->getChunks();
        $this->assertCount(3, $chunks);

<<<<<<< HEAD
        $this->assertEquals(20, $chunks[0]->getStart());
        $this->assertEquals(320, $chunks[1]->getStart());
        $this->assertEquals(600, $chunks[2]->getStart());
=======
        $this->assertSame(20, $chunks[0]->getStart());
        $this->assertSame(320, $chunks[1]->getStart());
        $this->assertSame(600, $chunks[2]->getStart());
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f

        $this->assertCount(5, $chunks[0]->getLines());
        $this->assertCount(5, $chunks[1]->getLines());
        $this->assertCount(4, $chunks[2]->getLines());
    }

<<<<<<< HEAD
    public function testParseWithRemovedLines()
    {
        $content = <<<A
=======
    public function testParseWithRemovedLines(): void
    {
        $content = <<<END
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
diff --git a/Test.txt b/Test.txt
index abcdefg..abcdefh 100644
--- a/Test.txt
+++ b/Test.txt
@@ -49,9 +49,8 @@
 A
-B
<<<<<<< HEAD
A;
        $diffs = $this->parser->parse($content);
        $this->assertInternalType('array', $diffs);
        $this->assertContainsOnlyInstancesOf('SebastianBergmann\Diff\Diff', $diffs);
=======
END;
        $diffs = $this->parser->parse($content);
        $this->assertContainsOnlyInstancesOf(Diff::class, $diffs);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        $this->assertCount(1, $diffs);

        $chunks = $diffs[0]->getChunks();

<<<<<<< HEAD
        $this->assertInternalType('array', $chunks);
        $this->assertContainsOnlyInstancesOf('SebastianBergmann\Diff\Chunk', $chunks);
=======
        $this->assertContainsOnlyInstancesOf(Chunk::class, $chunks);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        $this->assertCount(1, $chunks);

        $chunk = $chunks[0];
        $this->assertSame(49, $chunk->getStart());
        $this->assertSame(49, $chunk->getEnd());
        $this->assertSame(9, $chunk->getStartRange());
        $this->assertSame(8, $chunk->getEndRange());

        $lines = $chunk->getLines();
<<<<<<< HEAD
        $this->assertInternalType('array', $lines);
        $this->assertContainsOnlyInstancesOf('SebastianBergmann\Diff\Line', $lines);
=======
        $this->assertContainsOnlyInstancesOf(Line::class, $lines);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        $this->assertCount(2, $lines);

        /** @var Line $line */
        $line = $lines[0];
        $this->assertSame('A', $line->getContent());
        $this->assertSame(Line::UNCHANGED, $line->getType());

        $line = $lines[1];
        $this->assertSame('B', $line->getContent());
        $this->assertSame(Line::REMOVED, $line->getType());
    }

<<<<<<< HEAD
    public function testParseDiffForMulitpleFiles()
    {
        $content = <<<A
=======
    public function testParseDiffForMulitpleFiles(): void
    {
        $content = <<<END
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
diff --git a/Test.txt b/Test.txt
index abcdefg..abcdefh 100644
--- a/Test.txt
+++ b/Test.txt
@@ -1,3 +1,2 @@
 A
-B

diff --git a/Test123.txt b/Test123.txt
index abcdefg..abcdefh 100644
--- a/Test2.txt
+++ b/Test2.txt
@@ -1,2 +1,3 @@
 A
+B
<<<<<<< HEAD
A;
=======
END;
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        $diffs = $this->parser->parse($content);
        $this->assertCount(2, $diffs);

        /** @var Diff $diff */
        $diff = $diffs[0];
        $this->assertSame('a/Test.txt', $diff->getFrom());
        $this->assertSame('b/Test.txt', $diff->getTo());
        $this->assertCount(1, $diff->getChunks());

        $diff = $diffs[1];
        $this->assertSame('a/Test2.txt', $diff->getFrom());
        $this->assertSame('b/Test2.txt', $diff->getTo());
        $this->assertCount(1, $diff->getChunks());
    }
<<<<<<< HEAD
=======

    /**
     * @param string $diff
     * @param Diff[] $expected
     *
     * @dataProvider diffProvider
     */
    public function testParser(string $diff, array $expected): void
    {
        $result = $this->parser->parse($diff);

        $this->assertEquals($expected, $result);
    }

    public function diffProvider(): array
    {
        return [
            [
                "--- old.txt	2014-11-04 08:51:02.661868729 +0300\n+++ new.txt	2014-11-04 08:51:02.665868730 +0300\n@@ -1,3 +1,4 @@\n+2222111\n 1111111\n 1111111\n 1111111\n@@ -5,10 +6,8 @@\n 1111111\n 1111111\n 1111111\n +1121211\n 1111111\n -1111111\n -1111111\n -2222222\n 2222222\n 2222222\n 2222222\n@@ -17,5 +16,6 @@\n 2222222\n 2222222\n 2222222\n +2122212\n 2222222\n 2222222\n",
                \unserialize(FileUtils::getFileContent(__DIR__ . '/fixtures/serialized_diff.bin')),
            ],
        ];
    }
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
}
