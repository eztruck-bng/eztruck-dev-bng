<<<<<<< HEAD
<?php
=======
<?php declare(strict_types=1);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
/*
 * This file is part of sebastian/diff.
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\Diff;

use PHPUnit\Framework\TestCase;

/**
 * @covers SebastianBergmann\Diff\Chunk
 */
<<<<<<< HEAD
class ChunkTest extends TestCase
=======
final class ChunkTest extends TestCase
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
{
    /**
     * @var Chunk
     */
    private $chunk;

<<<<<<< HEAD
    protected function setUp()
=======
    protected function setUp(): void
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        $this->chunk = new Chunk;
    }

<<<<<<< HEAD
    public function testCanBeCreatedWithoutArguments()
    {
        $this->assertInstanceOf('SebastianBergmann\Diff\Chunk', $this->chunk);
    }

    public function testStartCanBeRetrieved()
    {
        $this->assertEquals(0, $this->chunk->getStart());
    }

    public function testStartRangeCanBeRetrieved()
    {
        $this->assertEquals(1, $this->chunk->getStartRange());
    }

    public function testEndCanBeRetrieved()
    {
        $this->assertEquals(0, $this->chunk->getEnd());
    }

    public function testEndRangeCanBeRetrieved()
    {
        $this->assertEquals(1, $this->chunk->getEndRange());
    }

    public function testLinesCanBeRetrieved()
    {
        $this->assertEquals(array(), $this->chunk->getLines());
    }

    public function testLinesCanBeSet()
    {
        $this->assertEquals(array(), $this->chunk->getLines());

        $testValue = array('line0', 'line1');
        $this->chunk->setLines($testValue);
        $this->assertEquals($testValue, $this->chunk->getLines());
=======
    public function testHasInitiallyNoLines(): void
    {
        $this->assertSame([], $this->chunk->getLines());
    }

    public function testCanBeCreatedWithoutArguments(): void
    {
        $this->assertInstanceOf(Chunk::class, $this->chunk);
    }

    public function testStartCanBeRetrieved(): void
    {
        $this->assertSame(0, $this->chunk->getStart());
    }

    public function testStartRangeCanBeRetrieved(): void
    {
        $this->assertSame(1, $this->chunk->getStartRange());
    }

    public function testEndCanBeRetrieved(): void
    {
        $this->assertSame(0, $this->chunk->getEnd());
    }

    public function testEndRangeCanBeRetrieved(): void
    {
        $this->assertSame(1, $this->chunk->getEndRange());
    }

    public function testLinesCanBeRetrieved(): void
    {
        $this->assertSame([], $this->chunk->getLines());
    }

    public function testLinesCanBeSet(): void
    {
        $lines = [new Line(Line::ADDED, 'added'), new Line(Line::REMOVED, 'removed')];

        $this->chunk->setLines($lines);

        $this->assertSame($lines, $this->chunk->getLines());
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    }
}
