<<<<<<< HEAD
<?php
=======
<?php declare(strict_types=1);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
/*
 * This file is part of sebastian/diff.
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\Diff;

<<<<<<< HEAD
class Diff
=======
final class Diff
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
{
    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    /**
     * @var Chunk[]
     */
    private $chunks;

    /**
     * @param string  $from
     * @param string  $to
     * @param Chunk[] $chunks
     */
<<<<<<< HEAD
    public function __construct($from, $to, array $chunks = array())
=======
    public function __construct(string $from, string $to, array $chunks = [])
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        $this->from   = $from;
        $this->to     = $to;
        $this->chunks = $chunks;
    }

<<<<<<< HEAD
    /**
     * @return string
     */
    public function getFrom()
=======
    public function getFrom(): string
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        return $this->from;
    }

<<<<<<< HEAD
    /**
     * @return string
     */
    public function getTo()
=======
    public function getTo(): string
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        return $this->to;
    }

    /**
     * @return Chunk[]
     */
<<<<<<< HEAD
    public function getChunks()
=======
    public function getChunks(): array
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        return $this->chunks;
    }

    /**
     * @param Chunk[] $chunks
     */
<<<<<<< HEAD
    public function setChunks(array $chunks)
=======
    public function setChunks(array $chunks): void
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        $this->chunks = $chunks;
    }
}
