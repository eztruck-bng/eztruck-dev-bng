<<<<<<< HEAD
<?php
=======
<?php declare(strict_types=1);
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
/*
 * This file is part of sebastian/diff.
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\Diff;

<<<<<<< HEAD
class Chunk
=======
final class Chunk
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
{
    /**
     * @var int
     */
    private $start;

    /**
     * @var int
     */
    private $startRange;

    /**
     * @var int
     */
    private $end;

    /**
     * @var int
     */
    private $endRange;

    /**
<<<<<<< HEAD
     * @var array
     */
    private $lines;

    /**
     * @param int   $start
     * @param int   $startRange
     * @param int   $end
     * @param int   $endRange
     * @param array $lines
     */
    public function __construct($start = 0, $startRange = 1, $end = 0, $endRange = 1, array $lines = array())
    {
        $this->start      = (int) $start;
        $this->startRange = (int) $startRange;
        $this->end        = (int) $end;
        $this->endRange   = (int) $endRange;
        $this->lines      = $lines;
    }

    /**
     * @return int
     */
    public function getStart()
=======
     * @var Line[]
     */
    private $lines;

    public function __construct(int $start = 0, int $startRange = 1, int $end = 0, int $endRange = 1, array $lines = [])
    {
        $this->start      = $start;
        $this->startRange = $startRange;
        $this->end        = $end;
        $this->endRange   = $endRange;
        $this->lines      = $lines;
    }

    public function getStart(): int
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        return $this->start;
    }

<<<<<<< HEAD
    /**
     * @return int
     */
    public function getStartRange()
=======
    public function getStartRange(): int
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        return $this->startRange;
    }

<<<<<<< HEAD
    /**
     * @return int
     */
    public function getEnd()
=======
    public function getEnd(): int
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        return $this->end;
    }

<<<<<<< HEAD
    /**
     * @return int
     */
    public function getEndRange()
=======
    public function getEndRange(): int
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        return $this->endRange;
    }

    /**
<<<<<<< HEAD
     * @return array
     */
    public function getLines()
=======
     * @return Line[]
     */
    public function getLines(): array
>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
    {
        return $this->lines;
    }

    /**
<<<<<<< HEAD
     * @param array $lines
     */
    public function setLines(array $lines)
    {
=======
     * @param Line[] $lines
     */
    public function setLines(array $lines): void
    {
        foreach ($lines as $line) {
            if (!$line instanceof Line) {
                throw new InvalidArgumentException;
            }
        }

>>>>>>> 5432644c86c489efa5753a5f7abc71a896fe101f
        $this->lines = $lines;
    }
}
