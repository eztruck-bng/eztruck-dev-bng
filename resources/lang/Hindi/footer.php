<?php

return array (
  'complaint' => 'शिकायत',
  'faq' => 'सामान्य प्रश्न',
  'policy' => 'गोपनीयता नीति',
  'refund' => 'धन वापसी नीति',
  'help' => 'मदद',
  'become_a_partner' => 'भागीदार बनें',
  'download_to_ride' => 'डाउनलोड करने के लिए सवारी',
  'download_to_drive' => 'ड्राइव पर डाउनलोड करें',
  'top_countries' => 'शीर्ष देश',
  'about_ilyft' => 'के बारे में',
  'why_ilyft' => 'क्यों',
  'how_it_works' => 'यह काम किस प्रकार करता है',
  'blog' => 'ब्लॉग',
  'terms_conditions' => 'नियम एवं शर्तें',
  'fare_estimation' => 'किराया अनुमान',
  'support' => 'सहयोग',
  'contact_us' => 'संपर्क करें',
  'signup' => 'साइन अप करें',
  'signup_for_ride' => 'वीडियो के लिए साइन अप करें',
);
