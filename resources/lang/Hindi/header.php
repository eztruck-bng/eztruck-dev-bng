<?php

return array (
  'li_how_it_works' => 'यह काम किस प्रकार करता है',
  'li_sign_in' => 'साइन इन करें',
  'li_became_a_partner' => 'भागीदार बनें',
  'signin' => 
  array (
    'title_driver' => 'चालक',
    'title_user' => 'उपयोगकर्ता',
    'signin' => 'साइन इन करें',
  ),
  'mylift' => 'eztruck',
  'logout' => 'लॉग आउट',
  'english-short' => 'ENG',
  'hindi-short' => 'हिन्दी',
  'odiya-short' => 'ଓଡିଆ',
  'ride_name' => 'सवारी',
  'ride' => 
  array (
    'overview' => 'अवलोकन',
    'safety' => 'सुरक्षा',
    'airports' => 'हवाई अड्डों',
    'cities' => 'शहरों',
    'fare_estimator' => 'किराया अनुमानक',
    'how_it_work' => 'यह काम किस प्रकार करता है',
  ),
  'drive_name' => 'चलाना',
  'drive' => 
  array (
    'overview' => 'अवलोकन',
    'requirements' => 'आवश्यकताएँ',
    'driver_app' => 'ड्राइवर ऐप',
    'vehicle_solutions' => 'वाहन समाधान',
    'safety' => 'सुरक्षा',
    'local_driver_guide' => 'स्थान चालक गाइड',
  ),
  'cabs_name' => 'ट्रकों',
  'Cabs' => 'ट्रकों',
);
