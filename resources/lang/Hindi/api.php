<?php

return array (
  'user' => 
  array (
    'incorrect_password' => 'गलत पासवर्ड',
    'password_updated' => 'पासवर्ड अपडेट किया गया',
    'location_updated' => 'स्थान अपडेट किया गया',
    'profile_updated' => 'प्रोफ़ाइल अपडेट',
    'user_not_found' => 'उपयोगकर्ता नहीं मिला',
    'not_paid' => 'उपयोगकर्ता ने भुगतान नहीं किया',
  ),
  'ride' => 
  array (
    'request_inprogress' => 'पहले से ही प्रगति में अनुरोध',
    'no_providers_found' => 'कोई ड्राइवर नहीं मिला',
    'request_cancelled' => 'आपकी सवारी रद्द हो गई',
    'already_cancelled' => 'पहले से ही सवारी रद्द',
    'already_onride' => 'पहले से ही आप ऑनराइड हैं',
    'provider_rated' => 'ड्राइवर रेटेड',
    'request_scheduled' => 'सवारी अनुसूचित',
    'request_already_scheduled' => 'सवारी पहले से ही अनुसूचित है',
  ),
  'something_went_wrong' => 'कुछ गलत हो गया',
  'logout_success' => 'सफलतापूर्वक लॉग आउट किया गया',
  'services_not_found' => 'सेवाएं नहीं मिलीं',
  'promocode_applied' => 'प्रोमोकोड लागू',
  'promocode_expired' => 'प्रोमोकोड की समय सीमा समाप्त हो गई',
  'promocode_already_in_user' => 'प्रोमोकोड पहले से ही उपयोग में है',
  'paid' => 'भुगतान सफल',
  'added_to_your_wallet' => 'अपने बटुए में जोड़ा',
  'push' => 
  array (
    'request_accepted' => 'आपकी सवारी एक चालक द्वारा स्वीकार की जाती है',
    'arrived' => 'ड्राइवर आपके स्थान पर पहुंचा',
    'incoming_request' => 'नई आने वाली सवारी',
    'added_money_to_wallet' => ' अपने बटुए में जोड़ा',
    'charged_from_wallet' => ' अपने वॉलेट से शुल्क लिया गया',
    'document_verfied' => 'आपके दस्तावेज़ सत्यापित हैं, अब आप अपना व्यवसाय शुरू करने के लिए तैयार हैं',
    'provider_not_available' => 'असुविधा समय के लिए क्षमा करें, आपका साथी या व्यस्त। कृपया कुछ देर बाद प्रयास करें',
    'user_cancelled' => 'उपयोगकर्ता ने सवारी रद्द कर दी',
    'provider_cancelled' => 'चालक ने सवारी रद्द कर दी',
    'schedule_start' => 'आपका शेड्यूल राइड शुरू कर दिया गया है',
    'drop' => 'ड्राइवर ने यूजर को गिरा दिया',
  ),
  'razorpay' => 'रज़ॉर्पय के साथ भुगतान सफल',
  'paypal' => 'पेपाल के साथ भुगतान सफल',
  'card' => 'कार्ड के साथ भुगतान सफल',
);
