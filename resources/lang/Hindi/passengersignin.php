<?php

return array (
  'signin' => 'साइन इन करें',
  'email' => 'ईमेल',
  'password' => 'कुंजिका',
  'next' => 'आगे',
  'dont_account' => 'खाता नहीं है?',
  'forgot_pass' => 'अपना कूट शब्द भूल गए?',
  'sign_by' => 'या उपयोग में साइन इन करें',
  'already_reg' => 'पहले से खाता है?',
  'sign_up' => 'साइन अप करें',
  'login' => 'लॉग इन करें',
  'name' => 'नाम',
  'phone' => 'फ़ोन नंबर',
  'confirm_pass' => 'पासवर्ड की पुष्टि कीजिये',
  'select_vehicle' => 'वाहन का चयन करें',
  'vehicle_number' => 'वाहन संख्या',
  'vehicle_model' => 'वाहन मॉडल',
  'click_here' => 'यहाँ क्लिक करें',
  'email_addr' => 'ईमेल पता',
  'reset_password' => 'पासवर्ड रीसेट',
);
