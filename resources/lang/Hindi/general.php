<?php

return array (
  'address' => 'पता',
  'faq' => 
  array (
    'head' => 'अक्सर पूछे जाने वाले प्रश्न!!',
    'para' => 'हमने सभी उत्तरों को जोड़ने की कोशिश की है लेकिन फिर भी अगर हम help@ilyft.care से संपर्क करने में संकोच नहीं करते हैं',
  ),
  'admin' => 'व्यवस्थापक',
  'all' => 'सब',
  'allcategory' => 'सभी श्रेणी',
  'buy-tickets' => 'टिकट खरीदो',
  'category' => 'वर्ग',
  'delivery' => 'वितरण',
  'details' => 'विवरण',
);
