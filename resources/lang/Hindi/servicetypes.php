<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Service Types Translations
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'MIN'           => 'प्रति मिनट मूल्य निर्धारण',
    'HOUR'          => 'प्रति घंटा की दर से',
    'DISTANCE'      => 'दूर मूल्य निर्धारण',
    'DISTANCEMIN'   => 'दूरी और प्रति मिनट मूल्य निर्धारण',
    'DISTANCEHOUR'  => 'दूरी और प्रति घंटा की दर से',

];
