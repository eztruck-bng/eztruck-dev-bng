<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'ଏହି ପରିଚୟପତ୍ରଗୁଡିକ ଆମର ରେକର୍ଡଗୁଡିକ ସହିତ ମେଳ ଖାଉ ନାହିଁ |',
    'throttle' => '
ବହୁତ ଲଗ୍ଇନ୍ ଚେଷ୍ଟା | ଦୟାକରି ପୁନର୍ବାର ଚେଷ୍ଟା କରନ୍ତୁ: ସେକେଣ୍ଡ ସେକେଣ୍ଡରେ |',

];
