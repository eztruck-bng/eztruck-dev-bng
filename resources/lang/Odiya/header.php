<?php

return array (
  'li_how_it_works' => 'ଏହା କିପରି କାମ କରେ',
  'li_sign_in' => 'ଚୁକ୍ତି କରିବା',
  'li_became_a_driver' => 'ଏକ ଅଂଶୀଦାର ହୁଅ |',
  'signin' => 
  array (
    'title_driver' => 'ଡ୍ରାଇଭର |',
    'title_user' => 'ଉପଯୋଗକର୍ତ୍ତା |',
    'signin' => 'ଚୁକ୍ତି କରିବା',
  ),
  'mylift' => 'eztruck',
  'logout' => 'ପ୍ରସ୍ଥାନ କର',
  'english-short' => 'ENG',
  'hindi-short' => 'हिन्दी',
  'odiya-short' => 'ଓଡିଆ',
  'ride_name' => 'ରଥଯାତ୍ରା |',
  'ride' => 
  array (
    'overview' => 'ସମୀକ୍ଷା',
    'safety' => 'ସୁରକ୍ଷା',
    'airports' => 'ବିମାନବନ୍ଦର',
    'cities' => 'ସହରଗୁଡିକ',
    'fare_estimator' => 'ଭଡା ଆକଳନକାରୀ |',
    'how_it_work' => 'ଏହା କିପରି କାମ କରେ |',
  ),
  'drive_name' => 'ଡ୍ରାଇଭ୍ କରନ୍ତୁ |',
  'drive' => 
  array (
    'overview' => 'ସମୀକ୍ଷା',
    'requirements' => 'ଆବଶ୍ୟକତା',
    'driver_app' => 'ଡ୍ରାଇଭର ଆପ୍ |',
    'vehicle_solutions' => 'ଯାନ ସମାଧାନ',
    'safety' => 'ସୁରକ୍ଷା',
    'local_driver_guide' => 'ଅବସ୍ଥାନ ଡ୍ରାଇଭର ଗାଇଡ୍ |',
  ),
  'cabs_name' => 'କ୍ୟାବସ୍',
  'Cabs' => 'କ୍ୟାବସ୍',
);
