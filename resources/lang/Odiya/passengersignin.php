<?php

return array (
  'signin' => 'ଚୁକ୍ତି କରିବା',
  'email' => 'ଇମେଲ୍ କରନ୍ତୁ |',
  'password' => 'ପାସୱାର୍ଡ',
  'next' => 'ପରବର୍ତ୍ତୀ',
  'dont_account' => 'ଏକ ଖାତା ନାହିଁ?',
  'forgot_pass' => 'ଗୁପ୍ତ ଶବ୍ଦ ଭୁଲି ଯାଇଛନ୍ତି?',
  'sign_by' => 'କିମ୍ବା ବ୍ୟବହାରରେ ସାଇନ୍ କରନ୍ତୁ |',
  'already_reg' => 'ପୂର୍ବରୁ ଆକାଉଣ୍ଟ୍?',
  'sign_up' => 'ସାଇନ୍ ଅପ୍ କରନ୍ତୁ',
  'login' => 'ଭିତରକୁ ଯାଉ',
  'name' => 'ନାମ',
  'phone' => 'ଫୋନ୍ ନମ୍ବର',
  'confirm_pass' => 'ପାସ୍ବାଡ଼ ନିସ୍ଚିତ କର',
  'select_vehicle' => 'ଯାନ ଚୟନ କରନ୍ତୁ |',
  'vehicle_number' => 'ଯାନ ସଂଖ୍ୟା',
  'vehicle_model' => 'ଯାନ ମଡେଲ୍ |',
  'click_here' => 'ଏଠି କ୍ଲିକ କରନ୍ତୁ',
  'email_addr' => 'ଈମୈଲ ଠିକଣା',
  'reset_password' => 'ପାସୱାର୍ଡ ପୁନ Res ସେଟ୍ କରନ୍ତୁ |',
);
