<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Service Types Translations
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'MIN'           => 'ପ୍ରତି ମିନିଟ୍ ମୂଲ୍ୟ',
    'HOUR'          => 'ପ୍ରତି ଘଣ୍ଟା ମୂଲ୍ୟ',
    'DISTANCE'      => 'ଦୂର ମୂଲ୍ୟ',
    'DISTANCEMIN'   => 'ଦୂରତା ଏବଂ ପ୍ରତି ମିନିଟ୍ ମୂଲ୍ୟ |',
    'DISTANCEHOUR'  => 'ଦୂରତା ଏବଂ ପ୍ରତି ଘଣ୍ଟା ମୂଲ୍ୟ',

];
