<?php

return array (
  'complaint' => 'ଅଭିଯୋଗ',
  'faq' => 'ଫ୍ୟାକ୍',
  'policy' => 'ଗୋପନୀୟତା ନୀତି',
  'refund' => 'ଫେରସ୍ତ ନୀତି',
  'help' => 'ସାହାଯ୍ୟ',
  'become_a_driver' => 'ଏକ ଅଂଶୀଦାର ହୁଅନ୍ତୁ |',
  'download_to_ride' => 'ରାଇଡ୍ କୁ ଡାଉନଲୋଡ୍ କରନ୍ତୁ |',
  'download_to_drive' => 'ଡ୍ରାଇଭ୍ କୁ ଡାଉନଲୋଡ୍ କରନ୍ତୁ |',
  'top_countries' => 'ଶୀର୍ଷ ଦେଶଗୁଡିକ |',
  'about_ilyft' => 'ଏଜଟ୍ରକ୍ ବିଷୟରେ |',
  'why_ilyft' => 'କାହିଁକି ଏଜଟ୍ରକ୍ |',
  'how_it_works' => 'ଏହା କିପରି କାମ କରେ |',
  'blog' => 'ବ୍ଲଗ୍',
  'terms_conditions' => 'ସର୍ତ୍ତାବଳୀ',
  'fare_estimation' => 'ଭଡା ଆକଳନ',
  'support' => 'ସମର୍ଥନ',
  'contact_us' => 'ଆମ ସହିତ ଯୋଗାଯୋଗ କରନ୍ତୁ |',
  'signup' => 'ସାଇନ୍ ଅପ୍ କରନ୍ତୁ',
  'signup_for_ride' => 'ରଥ ପାଇଁ ସାଇନ୍ ଅପ୍ କରନ୍ତୁ |',
);
