<?php

return array (
  'user' => 
  array (
    'incorrect_password' => 'ଭୁଲ ପାସୱାର୍ଡ |',
    'password_updated' => 'ପାସୱାର୍ଡ ଅପଡେଟ୍ ହୋଇଛି |',
    'location_updated' => 'ଅବସ୍ଥାନ ଅଦ୍ୟତନ ହୋଇଛି |',
    'profile_updated' => 'ପ୍ରୋଫାଇଲ୍ ଅଦ୍ୟତନ ହୋଇଛି |',
    'user_not_found' => 'ବ୍ୟବହାରକାରୀ ମିଳିଲା ନାହିଁ',
    'not_paid' => 'ଉପଯୋଗକର୍ତ୍ତା ଦେୟମୁକ୍ତ ନୁହଁନ୍ତି |',
  ),
  'ride' => 
  array (
    'request_inprogress' => 'ପୂର୍ବରୁ ଅନୁରୋଧ ଚାଲିଛି |',
    'no_providers_found' => 'କ No ଣସି ଡ୍ରାଇଭର ମିଳିଲା ନାହିଁ |',
    'request_cancelled' => 'ତୁମର ରାଇଡ୍ ବାତିଲ୍ |',
    'already_cancelled' => 'ପୂର୍ବରୁ ରାଇଡ୍ ବାତିଲ୍ ହୋଇଛି |',
    'already_onride' => 'ଆଗରୁ ତୁମେ ଅନ୍ରାଇଡ୍ |',
    'provider_rated' => 'ଡ୍ରାଇଭର ମୂଲ୍ୟାୟନ',
    'request_scheduled' => 'ରଥ ଅନୁସୂଚିତ',
    'request_already_scheduled' => 'ରାଇଡ୍ ପୂର୍ବରୁ ଅନୁସୂଚିତ ହୋଇଛି |',
  ),
  'something_went_wrong' => 'କିଛି ଭୁଲ ହୋଇ ଗଲା',
  'logout_success' => 'ସଫଳତାର ସହିତ ଲଗ୍ ଆଉଟ୍ |',
  'services_not_found' => 'ସେବା ମିଳିଲା ନାହିଁ |',
  'promocode_applied' => 'ପ୍ରୋମୋକୋଡ୍ ପ୍ରୟୋଗ |',
  'promocode_expired' => 'ପ୍ରୋମୋକୋଡ୍ ମିଆଦ ପୂର୍ଣ୍ଣ ହୋଇଛି |',
  'promocode_already_in_user' => 'ପ୍ରୋମୋକୋଡ୍ ପୂର୍ବରୁ ବ୍ୟବହାରରେ ଅଛି |',
  'paid' => 'ଦେୟ ସଫଳ |',
  'added_to_your_wallet' => 'ତୁମର ୱାଲେଟରେ ଯୋଡା ଯାଇଛି |',
  'push' => 
  array (
    'request_accepted' => 'ଆପଣଙ୍କର ରାଇଡ୍ ଡ୍ରାଇଭର ଦ୍ୱାରା ଗ୍ରହଣ କରାଯାଇଛି |',
    'arrived' => 'ଡ୍ରାଇଭର ଆପଣଙ୍କ ଅବସ୍ଥାନରେ ପହଞ୍ଚିଲା |',
    'incoming_request' => 'ନୂତନ ଆସୁଥିବା ରଥଯାତ୍ରା |',
    'added_money_to_wallet' => ' ତୁମର ୱାଲେଟରେ ଯୋଡା ଯାଇଛି |',
    'charged_from_wallet' => ' Charged from your Wallet',
    'document_verfied' => 'ତୁମର ଡକ୍ୟୁମେଣ୍ଟଗୁଡିକ ଯାଞ୍ଚ ହୋଇଛି, ବର୍ତ୍ତମାନ ତୁମେ ତୁମର ବ୍ୟବସାୟ ଆରମ୍ଭ କରିବାକୁ ପ୍ରସ୍ତୁତ |',
    'provider_not_available' => 'ଅସୁବିଧା ସମୟ ପାଇଁ ଦୁ Sorry ଖିତ, ଆପଣଙ୍କ ସାଥୀ କିମ୍ବା ବ୍ୟସ୍ତ | ଦୟାକରି କିଛି ସମୟ ପରେ ଚେଷ୍ଟା କରନ୍ତୁ |',
    'user_cancelled' => 'ଉପଭୋକ୍ତା ରଥକୁ ବାତିଲ କଲେ |',
    'provider_cancelled' => 'ଡ୍ରାଇଭର ରଥକୁ ବାତିଲ କରିଦେଲା |',
    'schedule_start' => 'ତୁମର କାର୍ଯ୍ୟସୂଚୀ ଯାତ୍ରା ଆରମ୍ଭ ହୋଇଛି |',
    'drop' => 'ଡ୍ରାଇଭର ଚାଳକକୁ ଛାଡିଦେଲା |',
  ),
  'razorpay' => 'ରେଜର୍ପେ ସହିତ ଦେୟ ସଫଳ |',
  'paypal' => 'ପେପାଲ ସହିତ ଦେୟ ସଫଳ |',
  'card' => 'କାର୍ଡ ସହିତ ଦେୟ ସଫଳ |',
);
