<?php

return array (
  'complaint' => 'شكوى',
  'faq' => 'لمعلوماتك ',
  'policy' => 'سياسة الخصوصية',
  'refund' => 'سياسة الاسترجاع',
  'help' => 'مساعدة',
  'support' => 'الدعم',
  'terms' => 'شروط الإستخدام',
  'become_a_driver' => 'تسجيل كسائق',
  'download_to_ride' => 'الوجهه',
  'download_to_drive' => 'نقطة الوصول ',
  'top_countries' => 'أهم الدول',
  'about_ilyft' => 'حول ilyft',
  'why_ilyft' => 'لماذا الحافة',
  'how_it_works' => 'كيف تعمل',
  'blog' => 'مدونة',
  'terms_conditions' => 'البنود و الظروف',
  'fare_estimation' => 'تقدير الأجرة',
  'contact_us' => 'اتصل بنا',
  'signup' => 'سجل',
  'signup_for_ride' => 'اشترك في الركوب',
);
