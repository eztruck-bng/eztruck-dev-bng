<?php

return array (
  'signin' => 'الدخول ',
  'email' => 'البريد الإلكتروني
',
  'password' => 'كلمه السر
',
  'next' => 'التالى',
  'dont_account' => 'ليس لديك حساب؟
',
  'forgot_pass' => 'نسيت رقمك السري؟
',
  'sign_by' => 'التسجيل بواسطة ',
  'already_reg' => 'بالفعل لديك حساب؟',
  'sign_up' => 'سجل',
  'login' => 'تسجيل الدخول
',
  'name' => 'اسم
',
  'phone' => 'رقم الهاتف
',
  'confirm_pass' => 'تأكيد كلمة المرور',
  'select_vehicle' => 'اختر مركبة
',
  'vehicle_number' => 'عدد المركبات
',
  'vehicle_model' => 'طراز السيارة
',
  'click_here' => 'انقر هنا
',
  'email_addr' => 'عنوان البريد الإلكتروني
',
  'reset_password' => 'إعادة تعيين كلمة المرور',
);
