<?php
   $locale = session()->get('locale');
   if($locale){
      $locale;
   }else{
      $locale = 'en';
   }
   $des = $locale.'_description';
   $type = $locale.'_type';
   $name = $locale.'_name';
   $meta_des = $locale.'_meta_description';
   $title = $locale.'_title';
   ?>
@extends('website.app')
@section('styles')

<link href="{{asset('asset/front/css/slick.css')}}" rel="stylesheet"/>
<link href="{{asset('asset/front/css/slick-theme.css')}}"/>



<style type="text/css">
<!-- Custom CSS -->

body{
			font-size: 12px;
			font-family: monospace;
			color:black;
		}

		.container-fluid{
			
			<!-- background:#d22027; -->
			border: 10px solid #000;
			display:cover;
		}

		
		.index-head{

			margin-top: 14rem;
		}
		.bi{
			margin-bottom: 13.5rem;
		}
		.index-head h3{

			font-size: 5rem;

		}
		.index-head h4{
			font-size: 4rem;

		}
		.index-input-fields{
			padding: 10px;
			margin-top: 20px;
		}
		
		.index-input-fields	input{
			 border:none;
			font-size: 2rem;
			padding:0px 10px 0px 10px;
			margin:20px 20px;
			
		}

		.index-input-1,	input:focus{
			outline: none;
		}
		.index-input-2,	input:focus{
			outline: none;
		}

		
		
		.index-input-1{
			background: #fff;
			border: 1px solid red;
			border-radius: 10px;
			margin-right: 10px;
		}
		.index-input-2{
			background: #fff;
			border: 1px solid red;
			border-radius: 10px;
			margin-left: 10px;
		}
		 .index-button-1{
			border-radius: 10px;
			background:#d22027;
			color:white;
			font-size:2rem;
			padding: 1rem 2rem 1rem 2rem;
		}
		i {
			color:white;font-size:24px;padding-left: 2rem;
		}
		

<!-- End -->

.book_now_wrap  .btn.btn-default.sid_tg {
    text-transform: uppercase;
    font-family: 'open_sansbold';
    font-size: 16px;
    color: #333333;
    background-color: #2bb673;
    border-color: #2bb673;
    border-radius: 0;
    width: 100%;
    text-align: left;
    height: 40px;
    line-height: 28px;
    position: relative;
}

.book_now_wrap figure {
	float: right;
}
.slick-prev{
	z-index: 0 !important;
}
.slick-next{
	z-index: 0 !important;
}
</style>
@endsection

@section('content')

   <div class="container-fluid">
<div class="bi">
	<div class="row justify-content-center">
		<div class="index-head">
			<h4>Get there</h4>
			<h3>your day belongs to you!!</h3>
		</div>
	</div>

	<div class="row justify-content-center index-input-fields">
		
			<div class="index-input-1">
				<i class='fas fa-map-marker-alt' style='color:#d22027; font-size:24px'></i>
				<input type="text" placeholder="Enter Pickup Location">
			</div>	
		
			<div class="index-input-2" style='color:#d22027; font-size:24px'>
				<i class="fas fa-map-marker-alt" style="color:#d22027;"></i>	
				<input type="text" placeholder="Enter Destination">
			</div>	
		
		
	</div>

	<div class="row justify-content-center index-input-fields">
		
			<div class="index-button-1"> <a href="#" style="text-decoration:none; color:white;"> get ezTruck <i class="fa fa-angle-double-right" id="r-arrow"></i></a></div>
			
			
			
		
		
	</div>
</div>

</div> 
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('asset/front/js/slick.min.js')}}"></script>
<script type="text/javascript">

  var  saveLocation =function() {
	   $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            type	:	'POST',
            url	    : 	site_url+'/saveLocationTemp',
            data	: 	$('#location_form').serialize(), 
        
            success	: 	function(json) {
               // form.find('.destination').after('<div class="final_estimated" style="font-weight: bold; color:black"><span class="pull-left">Estimated Fare</span><span class="pull-right">'+json.estimated_fare+'</span></div>');  
                //form.find('.car-detail').after('<div class="book_now_wrap"><button type="submit" class="btn btn-default sid_tg">Book Now <figure><img src="img/btn_arrow.png" alt="img"></figure></button></div>');
            }
          });
       }
   
   var ip_details = [];
   var current_latitude =  20.394987; //20.296059; 
   var current_longitude = 85.853745; //85.824539;
   
   $(document).ready(function() {
     $("#owl-demo2").owlCarousel({
       autoPlay: 3000,
       items :3,
       autoPlay:true,
       navigation:true,
       navigationText:true,
       pagination:true,
       itemsDesktop : [1350,3],
       itemsDesktop : [1199,2],
       itemsDesktopSmall : [991,1],
       itemsTablet: [767,1], 
       itemsMobile : [560,1] 
     });
   
   });     
   
   function disableEnterKey(e) {
       var key;
       if(window.e) {
           key = window.e.keyCode; // IE
       } else {
           key = e.which; // Firefox
       }
   
       if(key == 13)
           return e.preventDefault();
   }
   
    $('.pricing_left .car-radio').on('click', function() {
   var detail = $('.car-detail');
   detail.find('input[type=radio]').attr('checked');
   detail.find('.car_ser_type').removeClass('car_ser_type');
   detail.find('.img_color').removeClass('img_color');
   $(this).find('img').addClass('img_color');
   $(this).find('span').addClass('car_ser_type');
   $(this).find('input[type=radio]').attr('checked', 'checked');
   
   });
   
   $('.car-detail').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: false,
		swipeToSlide: true,
		infinite: true
	});

   $(function() {
	   
		$('#destination-input').focusout(function(){
		   //console.log('hhhhhhhhhhhhh.....');
			saveLocation();
		});
		
		 $('.pricing_left .car-radio').on('click', function() {
			 
            var radioValue = $("input[name='service_type']:checked").val();
            if(radioValue){
                saveLocation(); 
            }
        });
	   
	    $('#origin-input,#destination-input').on('change,focusout', function() {

			//console.log('reached.....');
			saveLocation();
			
		});
   $('#estimated_btn').click(function(e) {
    e.preventDefault();
    form = $(this).closest('form');
    form.find('.help-block, .final_estimated, .book_now_wrap').remove();
   
    var origion                 = form.find('#origin-input');
    var destinated              = form.find('#destination-input');
   
    var origin_latitude		    =	form.find('#origin_latitude').val();
    var origin_longitude	    =	form.find('#origin_longitude').val();
    var destination_latitude    =   form.find('#destination_latitude').val();
    var destination_longitude   =   form.find('#destination_longitude').val();
    var formData = form.serializeArray();
   
    if( origion.val().length === 0 ) {
        form.find('.pickup_location').append('<span class="help-block text-danger">Please add a pick up location! </span>');
        return false;
    }
    
    if( destinated.val().length === 0 ) {
        form.find('.destination').append('<span class="help-block text-danger">Please add a final location! </span>');
        return false;
    }
   
   
    if( origin_latitude.length !== 0 &&  origin_latitude.length !== 0  && origin_latitude.length !== 0  && origin_latitude.length !==  0 ) {
		 
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            type	:	'POST',
            url   : '{{ url("/get_fare") }}',
            data	: 	formData,         
            success	: 	function(json) {
                form.find('.destination').after('<div class="final_estimated" style="font-weight: bold; color:black"><span class="pull-left">Estimated Fare</span><span class="pull-right">'+json.estimated_fare+'</span></div>');  
                form.find('.car-detail').after('<div class="book_now_wrap"><button type="submit" class="btn btn-default sid_tg">Book Now <figure><img src="asset/front_dashboard/img/btn_arrow.png" alt="img"></figure></button></div>');
            }
            
          });
   }
   });
   
   });
   
</script>
<script type="text/javascript" src="{{ asset('asset/front/js/map.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY_WEB') }}&libraries=places&callback=initMap" async defer></script>
@endsection