@extends('provider.layout.app')

@section('content')

    <div class="col-md-12" style="margin-bottom: 20px">
    <div class="dash-content">
        <div class="row no-margin">
            <div class="col-md-12">
                <h4 class="page-title">Document</h4>
                <hr/>
            </div>
        </div>
            <div class="pro-dashboard-content">
                <div class="container-fluid">
                
                        <div class="manage-doc-content">
                            <div class="manage-doc-section">
                                <div class="manage-doc-section-content">
                                    @foreach($DriverDocuments as $Document)
                                    <div class="manage-doc-box row no-margin">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="manage-doc-box-left">
                                                <p class="manage-txt">{{ $Document->name }}</p>
                                                <p class="license">Expires: {{ $Provider->document($Document->id) ? $Provider->document($Document->id)->expires_at : 'N/A' }}</p>
                                                <p class="manage-badge {{ $Provider->document($Document->id) ? ($Provider->document($Document->id)->status == 'ASSESSING' ? 'yellow-badge' : 'green-badge') : 'red-badge'}}">
                                                {{ $Provider->document($Document->id) ? $Provider->document($Document->id)->status : 'MISSING' }}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="manage-doc-box-right text-right" style="margin-top: -17px;">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <form action="{{ route('provider.documents.update', $Document->id) }}" method="POST" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PATCH') }}
                                                        <div class="form-control" data-trigger="fileinput">
                                                            <span class="fileinput-filename"></span>
                                                        </div>
                                                        <span class="input-group-addon btn btn-default btn-file fileinput-exists btn-submit">
                                                            <button>
                                                                <i class="fa fa-check"></i>
                                                            </button>
                                                        </span>
                                                        <span class="input-group-addon btn btn-default btn-file">
                                                            
                                                            <input type="file" name="document" accept="application/pdf, image/*">
                                                        </span>
                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="manage-doc-section">
                                <div class="manage-doc-section-head row no-margin">
                                </div>

                                <div>
                                    
                                    @forelse($VehicleDocuments as $Document)
                                    <div class="manage-doc-box row no-margin" style="margin-top: -17px;">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="manage-doc-box-left">
                                                <p class="manage-txt">{{ $Document->name }}</p>
                                                <p class="license">Expires: {{ $Provider->document($Document->id) ? $Provider->document($Document->id)->expires_at : 'N/A' }}</p>
                                                <p class="manage-badge {{ $Provider->document($Document->id) ? ($Provider->document($Document->id)->status == 'ASSESSING' ? 'yellow-badge' : 'green-badge') : 'red-badge'}}">
                                                    {{ $Provider->document($Document->id) ? $Provider->document($Document->id)->status : 'MISSING' }}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="manage-doc-box-right text-right">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <form action="{{ route('provider.documents.update', $Document->id) }}" method="POST" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PATCH') }}
                                                        <div class="form-control" data-trigger="fileinput">
                                                            <span class="fileinput-filename"></span>
                                                        </div>
                                                        <span class="input-group-addon btn btn-default btn-file fileinput-exists btn-submit">
                                                            <button>
                                                                <i class="fa fa-check"></i>
                                                            </button>
                                                        </span>
                                                        <span class="input-group-addon btn btn-default btn-file">
                                                            
                                                            <input type="file" name="document" accept="application/pdf, image/*">
                                                        </span>
                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @empty
                                    
                                    @endforelse
                                </div>
                            </div>
                        </div>
                
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</div>            
@endsection

@section('styles')
<link href="{{ asset('asset/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
    .fileinput .btn-file {
        padding:0;
        background-color: #fff;
        border: 0;
        border-radius:0!important;
    }
    .fileinput .form-control {
        border: 0;
        box-shadow : none;
        border-left:0;
        border-right:5px;
    }
    .fileinput .upload-link {
        border:0;
        border-radius: 0;
        padding:0;
    }
    .input-group-addon.btn {
        background: #fff;
        border: 1px solid #37b38b;
        border-radius: 0; 
        padding: 10px;
        height: 40px;
        line-height: 20px;
    }
    .fileinput .fileinput-filename {
        font-size: 10px;
    }
    .fileinput .btn-submit {
        padding: 0;
    }
    .fileinput button {
        background-color: white;
        border: 0;
        padding: 10px;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('asset/js/jasny-bootstrap.min.js') }}"></script>
@endsection