<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
	
    <title>{{ Setting::get('site_title') }}</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset(Setting::get('site_icon')) }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Cabzify">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="http://97pixelsdev.com/cabzify/public/uploads/imgpsh_fullsize_anim.png" />
    
    <meta property="og:image:alt" content="http://97pixelsdev.com/cabzify/public/uploads/6f2fc9b052c1773c963a110b7caa92565afb2451.png" />
    <meta property="og:url" content="http://97pixelsdev.com/cabzify/privacy-policy" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ Setting::get('site_title') }}" />
    <meta property="fb:app_id" content="3127828963956256" />
    <meta property="og:description" content="{{ Setting::get('site_title') }}" />
	<link href="{{asset('asset/front/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('asset/front_dashboard/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{asset('asset/front_dashboard/css/custom.css') }}" rel="stylesheet">  
    <link href="{{asset('asset/front_dashboard/css/hamburgers.css') }}" rel="stylesheet">
    <link href="{{asset('asset/front_dashboard/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{asset('asset/front_dashboard/css/owl.theme.css') }}" rel="stylesheet">
	<link href="{{asset('asset/front/css/slick.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('asset/front/css/slick-theme.css')}}"/>
	<link href="{{asset('asset/front/css/dashboard-style.css')}}" rel="stylesheet">
    <link href="{{asset('asset/front/css/ismael.css')}}" rel="stylesheet">
   @yield('styles')
</head>
<body >
<div class="index">
	
	@include('website.header')
    
	@yield('content')
	
	@include('website.footer')
</div>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script>
		base_url = "{{ env('APP_URL') }}"; 
	</script>
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY_WEB') }}&callback=initMap">
    </script>
	<script>
		//alert("HELLO WELCOME  TO EZTRUCK DASHBOARD !!!!!!!!");
	</script>
	
	<script>
				function addFormatter (input, formatFn) {
				  let oldValue = input.value;
				 
				  const handleInput = event => {
					const result = formatFn(input.value, oldValue, event);
					if (typeof result === 'string') {
					  input.value = result;
					}
				   
					oldValue = input.value;
				  }

				  handleInput();
				  input.addEventListener("input", handleInput);
				}

				function regexPrefix (regex, prefix) {
				  return (newValue, oldValue) => regex.test(newValue) ? newValue : (newValue ? oldValue : prefix);
				}

				const input = document.getElementById('phone_number');
				addFormatter(input, regexPrefix(/^\+?91/, '+91'));
			</script>
			
			<script>
var map, infoWindow;
var geocoder;
     function initMap() {
       map = new google.maps.Map(document.getElementById('map'), {
         center: {lat: -34.397, lng: 150.644},
         zoom: 6
       });
       geocoder = new google.maps.Geocoder;
       infoWindow = new google.maps.InfoWindow;

         if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(function(position) {
           var pos = {
             lat: position.coords.latitude,
             lng: position.coords.longitude
           };
		 alert(pos[0]);
		 alert(pos[1]);
           infoWindow.setPosition(pos);
           infoWindow.setContent('Location found.');
           infoWindow.open(map);
           map.setCenter(pos);
         }, function() {
           handleLocationError(true, infoWindow, map.getCenter());
         });
       } else {
       
         handleLocationError(false, infoWindow, map.getCenter());
       }
	   
	   
     }
      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
       infoWindow.setPosition(pos);
       infoWindow.setContent(browserHasGeolocation ?
                             'Error: The Geolocation service failed.' :
                             'Error: Your browser doesn\'t support geolocation.');
       infoWindow.open(map);
     }
	 
	 
</script>
    <script src="{{ asset('asset/front_dashboard/js/jquery.min.js') }}"></script>
    <script src="{{ asset('asset/front_dashboard/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('asset/front_dashboard/js/owl.carousel.js') }}"></script>
    <script>
        $("#left_menu_open").click(function(){
            $("#left_menu").addClass("open");
        });
        $(".close").click(function(){
            $("#left_menu").removeClass("open");
        });
        
        $(".hamburger").click(function(){
            $(".side_menu").toggleClass("open");
			$(this).toggleClass("is-active");
        }); 
        $(document).click(function(event) {    

          //if you click on anything except the modal itself or the "open modal" link, close the modal
          if (!$(event.target).closest(".hamburger,.is-active").length) {
            $("body").find("#left_menu").removeClass("open");
            $("body").find(".hamburger").removeClass("is-active");
          }
        });
		var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};

		var	site_url ="{{url('/')}}";
  
    </script>

@yield('scripts')
   
</body>
</html>

